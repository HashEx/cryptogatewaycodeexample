package utils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Gleb Zykov on 18/07/2017.
 */
public class Formatter {

    public static final String format(LocalDateTime localDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return formatter.format(localDateTime);
    }

    public static String formatToBtcUp(BigDecimal value) {
        return value.setScale(8, BigDecimal.ROUND_UP).stripTrailingZeros().toPlainString();
    }

    public static String formatToBtcDown(BigDecimal value) {
        return value.setScale(8, BigDecimal.ROUND_DOWN).stripTrailingZeros().toPlainString();
    }

    public static String ratePresentation(BigDecimal value) {
        return value.setScale(2, BigDecimal.ROUND_HALF_UP).toString();
    }
}
