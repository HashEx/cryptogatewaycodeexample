import bcJsonRpc.BitcoindClientFactory;
import bcJsonRpc.BitcoindInterface;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import controllers.HomeController;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import play.Logger;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import services.AdminBotService;
import services.BtcService;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * This class is a Guice module that tells Guice how to bind several
 * different types. This Guice module is created when the Play
 * application starts.
 *
 * Play will automatically use any class called `Module` that is in
 * the root package. You can create modules in other locations by
 * adding `play.modules.enabled` settings to the `application.conf`
 * configuration file.
 */
public class Module extends AbstractModule {

    @Override
    public void configure() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://btc-e.nz")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        BtcService service = retrofit.create(BtcService.class);
        bind(BtcService.class).toInstance(service);
    }

    @Provides
    public AdminBotService provideAdminBotService(Config config) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(Logger::info);
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();
        Retrofit telegramAdminRetrofit = new Retrofit.Builder()
                .baseUrl(config.getString("config.telegramBotUrl"))
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return telegramAdminRetrofit.create(AdminBotService.class);
    }

    @Provides
    public BitcoindInterface getClient(Config config) throws IOException {
        BitcoindClientFactory clientFactory;
        String rpcUrl = "config.bitcoind.url";
        String userName = config.getString("config.bitcoind.username");
        String password = config.getString("config.bitcoind.password");
        Logger.info("connecting to bitcoind with username " + userName);

        clientFactory = new BitcoindClientFactory(
                new URL(config.getString(rpcUrl)),
                userName,
                password);
        BitcoindInterface client = clientFactory.getClient();
        if (client == null) {
            throw new RuntimeException("could not connect to bitcoind");
        }
        return client;
    }

    public Config getConfigFromFile() {
        return HomeController.getConfigFromFile();
    }

}
