package services;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Gleb Zykov on 11/07/2017.
 */
public interface BtcService {

    @GET("/api/3/ticker/btc_rur")
    Call<TickerInfo> getTickerInfo();

}
