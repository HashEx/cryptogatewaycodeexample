package services;

import akka.actor.ActorSystem;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import play.db.jpa.JPAApi;
import scala.concurrent.duration.Duration;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by kataloo on 7/17/17.
 */
@Singleton
public class ConfirmationsCheckerService {

    private JPAApi jpaApi;
    private OrderService orderService;
    private ActorSystem actorSystem;

    @Inject
    public ConfirmationsCheckerService(OrderService orderService, JPAApi jpaApi,
                                       ActorSystem actorSystem) {
        this.jpaApi = jpaApi;
        this.orderService = orderService;
        this.actorSystem = actorSystem;
        actorSystem.scheduler().schedule(Duration.Zero(),
                Duration.create(15, TimeUnit.SECONDS),
                this::run,
                actorSystem.dispatcher());
    }

    public void run() {
        orderService.getNotPayedOrders().forEach(order -> {
            try {
                orderService.checkStatusUpdate(order);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

}
