package services;

import bcJsonRpc.pojo.Transaction;
import com.typesafe.config.Config;
import models.FsOrder;
import models.PaymentStatus;
import org.springframework.stereotype.Service;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

/**
 * Created by Gleb Zykov on 10/07/2017.
 */
@Service
public class OrderService {

    private JPAApi jpaApi;

    public static int RATE_FIX_PERIOD_MIN = 15;

    private final RateService rateService;
    private final BlockchainService blockchainService;
    private final AdminBotService adminBotService;
    private final Config config;

    @Inject
    public OrderService(JPAApi jpaApi,
                        RateService rateService,
                        Config config,
                        AdminBotService adminBotService,
                        Provider<BlockchainService> blockchainService) {
        this.jpaApi = jpaApi;
        this.config = config;
        this.rateService = rateService;
        this.adminBotService = adminBotService;
        this.blockchainService = blockchainService.get();
        this.blockchainService.setOrderService(this);
    }

    public Optional<FsOrder> getById(Integer id) {
        return jpaApi.withTransaction((entityManager) -> {
            FsOrder order = entityManager.find(FsOrder.class, id);
            return Optional.ofNullable(order);
        });
    }

    public boolean hasOrder(Integer id) {
        return getById(id).isPresent();
    }

    public void createOrder(FsOrder fsOrder) {
        jpaApi.withTransaction(() -> {
            if(getById(fsOrder.getId()).isPresent()) {
                //do nothing
            } else {
                EntityManager em = jpaApi.em();
                em.persist(fsOrder);
            }
            try {
                String orderUrl = config.getString("config.serverUrl") + "/order/" + fsOrder.getId();
                String message = "Создан заказ " + fsOrder.getId() + " на сумму " +
                        fsOrder.getSum().divide(BigInteger.TEN).divide(BigInteger.TEN) +
                        " руб. " + orderUrl;
                adminBotService.sendMessage(message).execute();
            } catch (Exception ex) {
                Logger.error("could not send message", ex);
            }
        });
    }

    private void  updateRateIfNeeded(FsOrder order) throws IOException {
        if(shouldUpdateRate(order)) {
            order.setRate(rateService.getRate());
            order.setRateFixTime(LocalDateTime.now());
            jpaApi.withTransaction(() -> jpaApi.em().merge(order));
        }
    }

    public synchronized void checkStatusUpdate(FsOrder order) throws IOException {
        Logger.info("checking status update for order {}", order.getId());
        BigDecimal unconfirmed = blockchainService.getUnconfirmedForOrder(order);
        BigDecimal confirmed = blockchainService.getConfirmedForOrder(order);
        BigDecimal sumInBtc = calculateSumInBitcoin(order);
        updateStatus(order.getId(), getStatusForSums(sumInBtc, unconfirmed, confirmed));
    }

    static PaymentStatus getStatusForSums(BigDecimal sum, BigDecimal unconfirmed, BigDecimal confirmed) {
        sum = sum.setScale(8, BigDecimal.ROUND_UP);
        if (unconfirmed.doubleValue() <= 0) {
            return PaymentStatus.CREATED;
        } else if (confirmed.compareTo(sum) > 0) {
            return PaymentStatus.CONFIRMED;
        } else if (unconfirmed.compareTo(sum) == 0) {
            return PaymentStatus.RECEIVED;
        } else {
            return PaymentStatus.CREATED;
        }
    }

    public BigDecimal calculateSumInBitcoin(FsOrder order) throws IOException {
        updateRateIfNeeded(order);
        return BigDecimal.valueOf(order.getSum().intValue())
                .divide(order.getRate().multiply(BigDecimal.valueOf(100)), MathContext.DECIMAL128);
    }

    private void updateStatus(int orderId, PaymentStatus status) {
        jpaApi.withTransaction(entityManager -> {
            EntityManager em = jpaApi.em();
            FsOrder fsOrder = em.find(FsOrder.class, orderId);
            if (fsOrder != null) {
                if (fsOrder.getPaymentStatus() != status) {
                    Logger.info("updating order {} status to {}", orderId, status);
                    fsOrder.setPaymentStatus(status);
                    em.merge(fsOrder);

                    try {
                        String orderUrl = config.getString("config.serverUrl") + "/order/" + orderId;
                        String message = "Заказ " + orderId + " переведен в статус " + status.getDescription() +
                                " " + orderUrl;
                        if(status == PaymentStatus.RECEIVED || status == PaymentStatus.CONFIRMED) {
                            List<Transaction> transactions = blockchainService.getTransactions(orderId);
                            String txListStr = transactions.stream()
                                    .map(Transaction::getTxid)
                                    .reduce("", (a, b) -> a + ", " + b);
                            txListStr = txListStr.substring(1, txListStr.length());
                            message = message + "\nТранзакции: " + txListStr;
                        }
                        adminBotService.sendMessage(message).execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        });
    }

    /**
     * Получение списка неоплаченных заказов
     * @return список неоплаченных заказов
     */
    List<FsOrder> getNotPayedOrders() {
        return jpaApi.withTransaction(() -> {
            EntityManager em = jpaApi.em();
            TypedQuery<FsOrder> query = em.createQuery("from FsOrder where paymentstatus != :status", FsOrder.class);
            query.setParameter("status", PaymentStatus.CONFIRMED.toString());
            return query.getResultList();
        });
    }

    private boolean shouldUpdateRate(FsOrder order) {
        if (order.getPaymentStatus() == PaymentStatus.CREATED) {
            return order.getRate() == null ||
                    LocalDateTime.now().isAfter(order.getRateFixTime().plus(RATE_FIX_PERIOD_MIN, ChronoUnit.MINUTES));
        } else {
            return false;
        }
    }

}
