package services;

/**
 * Created by Gleb Zykov on 11/07/2017.
 */
public class TickerInfo {

    private Values btc_rur;

    public Values getBtc_rur() {
        return btc_rur;
    }

    public void setBtc_rur(Values btc_rur) {
        this.btc_rur = btc_rur;
    }

    public static class Values {

        private double high;
        private double low;
        private double avg;
        private double buy;
        private double sell;

        public double getSell() {
            return sell;
        }

        public void setSell(double sell) {
            this.sell = sell;
        }

        public double getHigh() {
            return high;
        }

        public void setHigh(double high) {
            this.high = high;
        }

        public double getLow() {
            return low;
        }

        public void setLow(double low) {
            this.low = low;
        }

        public double getAvg() {
            return avg;
        }

        public void setAvg(double avg) {
            this.avg = avg;
        }

        public double getBuy() {
            return buy;
        }

        public void setBuy(double buy) {
            this.buy = buy;
        }

    }

}
