package services;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Gleb Zykov on 14/07/2017.
 */
public interface AdminBotService {

    @GET("sendMessages")
    Call<Void> sendMessage(@Query("message") String message);

}
