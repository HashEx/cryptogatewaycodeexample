package services;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import play.Logger;
import play.libs.Json;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Gleb Zykov on 11/07/2017.
 */
@Service
public class RateService {

    private final SettingsService settingsService;

    @Inject
    public RateService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    public BigDecimal getRate() throws IOException {
        try {
            double commission = settingsService.getSettings().getCommission();
            BigDecimal finalUsdRate = getEffectiveUsdRubRate();
            return finalUsdRate
                    .multiply(getPoloniexUsdtToBtcRate())
                    .multiply(BigDecimal.valueOf(1 - (commission / 100)));
        } catch (ParserConfigurationException | SAXException e) {
            e.printStackTrace();
            throw new IOException("could not calculate rate");
        }
    }

    public BigDecimal getRubToUsdFromCbXml() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        String today = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        Document document = documentBuilder.parse("https://www.cbr.ru/scripts/XML_daily.asp?date_req="+ today);
        NodeList valCurs = document.getElementsByTagName("Valute");
        for(int i = 0; i < valCurs.getLength(); i++) {
            Node node = valCurs.item(i);
            if(node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                String attribute = element.getAttribute("ID");
                if("R01235".equals(attribute)) {
                    String value = element.getElementsByTagName("Value")
                            .item(0)
                            .getTextContent()
                            .replace(",", ".");
                    return BigDecimal.valueOf(Double.parseDouble(value));
                }
            }
        }
        throw new IOException("Не удалось получить курс");
    }

    public BigDecimal getPoloniexUsdtToBtcRate() {
        try {
            Logger.info("request poloniex ticker");
            String response = HttpRequest
                    .get("https://poloniex.com/public?command=returnTicker")
                    .body();
            PoloniexResponse poloniexResponse = Json.fromJson(Json.parse(response), PoloniexResponse.class);
            return poloniexResponse.btcToUsd.highestBid;
        } catch (Exception ex) {
            Logger.warn("could not get poloniex ticker", ex);
            throw ex;
        }
    }

    public BigDecimal getEffectiveUsdRubRate() throws IOException, SAXException, ParserConfigurationException {
        BigDecimal usdRate = getRubToUsdFromCbXml();
        Double rubUsdCommission = settingsService.getSettings().getRubUsdCommission();
        return usdRate.multiply(BigDecimal.valueOf(1 - (rubUsdCommission / 100)));
    }

    public static class PoloniexResponse {
        @JsonProperty("USDT_BTC")
        public TickerInfo btcToUsd;
    }

    public static class TickerInfo {
        public String id;
        public BigDecimal last;
        public BigDecimal lowestAsk;
        public BigDecimal highestBid;
    }

}
