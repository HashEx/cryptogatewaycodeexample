package services;

import bcJsonRpc.BitcoindInterface;
import bcJsonRpc.events.AlertListener;
import bcJsonRpc.events.BlockListener;
import bcJsonRpc.events.WalletListener;
import bcJsonRpc.pojo.Block;
import bcJsonRpc.pojo.Transaction;
import models.FsOrder;
import play.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Gleb Zykov on 14/07/2017.
 */
@Singleton
public class BlockchainService {

    public static final int MINIMUM_CONFIRMATIONS = 6;
    private BitcoindInterface client;

    private OrderService orderService;

    @Inject
    public BlockchainService(BitcoindInterface client) {
        this.client = client;
        start();
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public String getAddressForOrder(FsOrder order) {
        return client.getaccountaddress(getAccountNameForOrder(order));
    }

    public BigDecimal getUnconfirmedForOrder(FsOrder order) {
        return client.getbalance(getAccountNameForOrder(order), 0);
    }

    public BigDecimal getConfirmedForOrder(FsOrder order) {
        return client.getbalance(getAccountNameForOrder(order), MINIMUM_CONFIRMATIONS);
    }

    public BigDecimal getBalance() {
        return client.getbalance();
    }

    public void sendToAddress(String address, BigDecimal amount) {
        client.sendtoaddress(address, amount);
    }

    public List<Transaction> getTransactions(long orderId) {
        return client.listtransactions(String.valueOf(orderId), 5, 0);
    }

    private String getAccountNameForOrder(FsOrder fsOrder) {
        return String.valueOf(fsOrder.getId());
    }

    private void start() {
        Logger.info("starting updates listeners");
        try {
            new BlockListener(client).addObserver((o, arg) -> {
                Block block = (Block)arg;
                System.out.println("got block " + block);
            });

            new WalletListener(client).addObserver((o, arg) -> {
                System.out.println("got wallet update " + arg);
                if(arg != null && arg instanceof Transaction) {
                    Transaction transaction = (Transaction) arg;
                    transaction.getDetails().forEach(this::handleTransactionUpdate);
                }
            });

            new AlertListener().addObserver((o, arg) -> {
                System.out.println("got alert " + arg);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handleTransactionUpdate(Transaction transaction) {
        if(transaction != null && transaction.getAccount() != null) {
            try {
                Integer orderId = Integer.parseInt(transaction.getAccount());
                orderService.getById(orderId).ifPresent(order -> {
                    try {
                        orderService.checkStatusUpdate(order);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }
        }
    }
}
