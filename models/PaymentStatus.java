package models;

/**
 * Created by Gleb Zykov on 12/07/2017.
 */
public enum PaymentStatus {

    CREATED, RECEIVED, CONFIRMED;

    public String getDescription() {
        switch (this) {
            case CREATED:
                return "Ожидание оплаты";
            case RECEIVED:
                return "Ожидание подтверждения";
            case CONFIRMED:
                return "Оплачен";
            default:
                throw new RuntimeException("unhandled status " + this);
        }
    }

}
