package models;

import play.Logger;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;

/**
 * Created by Gleb Zykov on 10/07/2017.
 * Заказ
 */
@Entity
public class FsOrder {

    @Id
    @Column(unique = true)
    private Integer id;

    private BigInteger sum;

    private LocalDateTime rateFixTime;

    private BigDecimal rate;

    @Enumerated(EnumType.STRING)
    private PaymentStatus paymentStatus;

    public FsOrder() {
        paymentStatus = PaymentStatus.CREATED;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * получить сумму в копейках
     * @return сумма заказа в копейках
     */
    public BigInteger getSum() {
        return sum;
    }

    public BigInteger getSumInRubles() {
        return sum.divide(BigInteger.valueOf(100));
    }

    public void setSum(BigInteger sum) {
        this.sum = sum;
    }

    public LocalDateTime getRateFixTime() {
        return rateFixTime;
    }

    public void setRateFixTime(LocalDateTime rateFixTime) {
        this.rateFixTime = rateFixTime;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    @Override
    public String toString() {
        return "FsOrder{" +
                "id=" + id +
                ", sum=" + sum +
                ", rateFixTime=" + rateFixTime +
                ", rate=" + rate +
                ", paymentStatus=" + paymentStatus +
                '}';
    }
}
