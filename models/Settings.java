package models;

import play.data.validation.Constraints;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Gleb Zykov on 19/07/2017.
 */
@Entity
public class Settings {

    @Id
    @Column(unique = true)
    private Integer id;

    @Constraints.Required
    @Constraints.Min(0)
    private Double commission;


    @Constraints.Min(0)
    private Double rubUsdCommission;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getCommission() {
        if (commission == null) {
            return 0d;
        }
        return commission;
    }

    /**
     * комиссия конвертации рубль - доллар
     */
    public Double getRubUsdCommission() {
        if (rubUsdCommission == null) {
            return 0d;
        }
        return rubUsdCommission;
    }

    public void setRubUsdCommission(Double rubUsdCommission) {
        this.rubUsdCommission = rubUsdCommission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    @Override
    public String toString() {
        return "Settings{" +
                "id=" + id +
                ", commission=" + commission +
                ", rubUsdCommission=" + rubUsdCommission +
                '}';
    }
}
