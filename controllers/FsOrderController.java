package controllers;

import com.google.inject.Inject;
import com.typesafe.config.Config;
import models.FsOrder;
import play.libs.Json;
import play.mvc.Result;
import services.BlockchainService;
import services.BtcService;
import services.OrderService;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import static play.mvc.Results.*;

/**
 * Created by Gleb Zykov on 10/07/2017.
 * Заказы
 */
public class FsOrderController {

    private OrderService orderService;
//    private final BitcoindInterface client;
    private final BtcService btcService;
    private final BlockchainService blockchainService;
    private final Config config;

    @Inject
    public FsOrderController(OrderService orderService,
                             BtcService btcService,
                             BlockchainService listener,
                             Config config) throws IOException {
        this.orderService = orderService;
        this.blockchainService = listener;
        this.btcService = btcService;
        this.config = config;
    }

    public Result orderInfo(int id) {
        Optional<FsOrder> fsOrderOpt = orderService.getById(id);
        if(!fsOrderOpt.isPresent()) {
            return notFound("заказ не найден");
        }
        FsOrder order = fsOrderOpt.get();
        String accountAddress = blockchainService.getAddressForOrder(order);
        BigDecimal unconfirmed = blockchainService.getUnconfirmedForOrder(order);
        BigDecimal confirmed = blockchainService.getConfirmedForOrder(order);
        BigDecimal bitcoinSum;
        try {
            bitcoinSum = orderService.calculateSumInBitcoin(order);
            orderService.checkStatusUpdate(order);
        } catch (IOException e) {
            e.printStackTrace();
            return internalServerError("не удалось обновить курс");
        }

        long until = -1 * order.getRateFixTime().plusMinutes(15).until(LocalDateTime.now(), ChronoUnit.SECONDS);
        return ok(views.html.orderInfo.render(until, order,
                accountAddress,
                null,
                bitcoinSum,
                unconfirmed,
                confirmed
        ));
    }

    public Result newOrder() {
        FsOrder order = new FsOrder();
        order.setId(1);
        order.setSum(BigInteger.valueOf(100));
        orderService.createOrder(order);
        String url = config.getString("config.serverUrl") + "/order/" + order.getId();
        return ok(Json.toJson(new AddOrderResponse(url)));
    }

    public Result newOrder(int id, long sum) {
        if(orderService.hasOrder(id)) {
            return badRequest("Заказ уже существует");
        }
        FsOrder order = new FsOrder();
        order.setId(id);
        order.setSum(BigInteger.valueOf(sum));
        orderService.createOrder(order);
        String url = config.getString("config.serverUrl") + "/order/" + order.getId();
        return ok(Json.toJson(new AddOrderResponse(url)));
    }

    public static class AddOrderResponse {

        public String url;

        public AddOrderResponse(String url) {
            this.url = url;
        }
    }

}
