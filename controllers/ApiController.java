package controllers;

import com.google.inject.Inject;
import play.Logger;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.Controller;
import services.BlockchainService;

import java.math.BigDecimal;

/**
 * Created by kataloo on 8/12/17.
 */
public class ApiController extends Controller {

    private final BlockchainService blockchainService;
//    private static final String address = "n3ryXr77FTpdQBAGNirzkS4MCFqUqBfTCe"; //test address
    private static final String address = "1B3sXptHXy19nQn5YEcau7zj8AZeodMHAe"; //production address

    @Inject
    public ApiController(BlockchainService service) {
        this.blockchainService = service;
    }

    public Result getBalance() {
        BigDecimal balance = blockchainService.getBalance();
        return okResponse(balance.toPlainString());
    }

    public Result send(Double amount) {
        try {
            blockchainService.sendToAddress(address, BigDecimal.valueOf(amount));
        } catch (Exception ex) {
            Logger.warn("could not send to address", ex);
            return badRequest(ex.getMessage());
        }
        return okResponse("средства высланы");
    }

    public Result okResponse(String text) {
        return ok(Json.toJson(new ServerResponse(text)));
    }

    public static class ServerResponse {
        public String text;

        public ServerResponse(String text) {
            this.text = text;
        }
    }
}
